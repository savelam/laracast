<?php

namespace Api\Transformers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LessonTransformer extends Transformer {

    public function transform($lession) {
        //return $lession;
        return [
            'id' => $lession["id"],
            'title' => $lession['title'],
            'body' => $lession['body'],
            'active' => (boolean) $lession['some_bool'],
        ];
    }

}

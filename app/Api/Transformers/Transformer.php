<?php

namespace Api\Transformers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Transformer {

    /**
     * Transform a collection of lessons
     * @param type $items
     * @return array
     */
    public function transformCollection(array $items) {
        return array_map([$this, 'transform'], $items);
    }

    
    public abstract function transform($item);
}

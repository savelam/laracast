<?php

namespace App\Http\Controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiController
 *
 * @author dela
 */
class ApiController extends Controller {

    //put your code here
    /**
     *
     * @var type 
     */
    protected $statusCode = 200;

    /**
     * 
     * @return type
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * 
     * @param type $statusCode
     */
    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * 
     * @param type $message
     * @return type
     */
    public function respondNotFound($message = 'Not Found!') {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Error') {
        return $this->setStatusCode(500)->respondWithError($message);
    }
    public function respondCreated($message = 'Created') {
        return $this->setStatusCode(201)->respondWithError($message);
    }
    
    public function respondValidationError($message = 'Validation Error') {
        return $this->setStatusCode(422)->respondWithError($message);
    }

    /**
     * 
     * @param type $data
     * @param type $headers
     * @return type
     */
    public function respond($data, $headers = []) {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * 
     * @param type $message
     * @return type
     */
    public function respondWithError($message) {
        return $this->respond([
                    'error' => [
                        'message' => $message,
                        'status_code' => $this->statusCode
                    ]
        ]);
    }

}

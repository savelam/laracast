<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index(){
        $lessons=['my first lession','my second lesson','my third lesson'];
        $name="Lamadeku Saviour";
        return view('pages.home')->with(['lessons'=>$lessons,'name'=>$name]);
    }
    //
    public function about(){
        return view('pages.about');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Lesson;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Api\Transformers\LessonTransformer;
use Illuminate\Pagination;

class LessonsController extends ApiController {

    //
    protected $lessonTransformer;

    public function __construct(LessonTransformer $lessonTransformer) {
        $this->lessonTransformer = $lessonTransformer;
        $this->middleware('auth.basic', ['on' => 'post']);
    }

    /**
     * 
     * @return type
     */
    public function index() {
//        return \App\Lesson::paginate(4);
        $limit = Input::get('limit')? : 4;
        $lessons = Lesson::paginate($limit);
        $lesson2 = $lessons->toArray();
        return $this->respond([
                    'data' => $this->lessonTransformer->transformCollection($lesson2['data']),
                    'paginator' => [
                        'total_count' => $lessons->total(),
                        'total_pages' => ceil($lessons->total()/$lessons->perPage()),
                        'current_page' => $lessons->currentPage(),
                        'limits' => $lessons->perPage(),
                    ]
        ]);
    }

    public function show($id) {
        $lessons = Lesson::find($id);
        if (!$lessons) {
//            return response()->json([
//                        'error' => [
//                            'message' => 'Lesson does not exist'
//                        ]
//                            ], 404);
//        }
            return $this->respondNotFound('Lessions Not found');
        }
        return $this->respond([
                    'data' => $this->lessonTransformer->transform($lessons)
        ]);
    }

    // function to store a new lesson via API
    public function store(Request $request) {

        if (!$request->input('title') || !$request->input('body')) {
            return $this->respondValidationError('Parameters not set');
        }
        Lesson::create($request->all());
        return $this->respondCreated('Lesson succefully created!');
    }

}

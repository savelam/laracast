<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Person;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class PersonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Person $person)
    {
        $person = $person->get();
       // dd($person);

        return view('directory.person.index', compact('person'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('directory.person.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request,Person $person)
    {
        
        $person->create($request->all());

        Session::flash('flash_message', ' added!');

//        return redirect('person');
        dd('added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id,Person $person)
    {
        $person = $person->findOrFail($id);

        return view('directory.person.show', compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id,Person $person)
    {
        $person = $person->findOrFail($id);

        return view('directory.person.edit', compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request,Person $person)
    {
        
        $person = $person->findOrFail($id);
        $person->update($request->all());

        Session::flash('flash_message', ' updated!');

        return "redirect('person')";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id,Person $person)
    {
        $person->destroy($id);

        Session::flash('flash_message', ' deleted!');

        return redirect('person');
    }

}

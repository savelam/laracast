<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Song;
use App\Http\Requests\CreateSongResquest;

class SongsController extends Controller {

    //

    public function index(Song $song) {
        $songs = $song->get();
        return view('songs.index')->with('songs', $songs);
    }

    // 
    public function show(Song $song) {
        //$song=$song->where('slug',$slug)->first();
        return view('songs.show')->with('song', $song);
    }
    
    /*
    function to create a view for songs creation
     *      */
    public function create() {
        return view('songs.create');
    }
    
    public function store(Song $song,  CreateSongResquest $request) {
        //dd($request->all());
        $song->create( $request->all());
        return redirect()->route('songs_path');
    }

    public function edit(Song $song) {

        //$song=$song->where('slug',$slug)->first();
        return view('songs.edit')->with('song', $song);
    }

    public function update(Song $song,Request $request){
        //$song=$song->where('slug',$slug)->first();
        $song->fill($request->input());  // for all the fillable inputs
        $song->save();
        return redirect('songs');
    }
    
    public function destroy(Song $song){
        $song->delete();
        return redirect('songs');
    }
}

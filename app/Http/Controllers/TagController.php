<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Lesson;
use Api\Transformers\TagTransformer

;

class TagController extends ApiController {

    protected $tagTransformer;

    public function __construct(TagTransformer $tagTransformer) {
        $this->tagTransformer = $tagTransformer;
//        $this->middleware('auth.basic', ['on' => 'post']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lessonId=null) {
        //
        $tags = $lessonId ? Lesson::findorFail($lessonId)->tags:Tag::all();
        if(!$tags){
           return $this->respondNotFound('Tag Not found'); 
        }
        return $this->respond([
                    'data' => $this->tagTransformer->transformCollection($tags->toArray())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //

        if (!$request->input('name')) {
            return $this->respondValidationError('Parameters not set');
        }
        Tag::create($request->all());
        return $this->respondCreated('Tag succefully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $tag = Tag::find($id);
        if (!$tag) {
            return $this->respondNotFound('Tag Not found');
        }
        return $this->respond([
                    'data' => $this->tagTransformer->transform($tag)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}

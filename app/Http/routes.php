<?php

use App\Task;
use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
// Route Model Binding
Route::bind('songs', function($slug) {
    return App\Song::where('slug', $slug)->first();
});  // ROUTE MODEL BINDING
//Route::get('home', 'HomeController@index');
//Route::get('about', 'HomeController@about');
Route::get('/', 'SongsController@index');
//Route::get('songs/{song}', 'SongsController@show');
//Route::get('songs/{song}/edit', 'SongsController@edit');
//Route::patch('songs/{song}', 'SongsController@update');

/*
  USING ROUTE RESOURCE
 *  */


/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    //
    Route::resource('songs', 'SongsController', [
        'names' => [
            'index' => 'songs_path',
            'show' => 'song_path',
            'update' => 'update_path',
            'store' => 'store_path',
            'destroy' => 'delete_path',
        ]
    ]);

    /**
     * Show Task Dashboard
     */
    Route::auth();

    Route::get('/tasks', 'TaskController@index');
    Route::post('/task', 'TaskController@store');
    Route::delete('/task/{task}', 'TaskController@destroy');
    
    Route::group(['prefix'=>'api/v1'],function(){
        Route::get('lessons/{id}/tags','TagController@index');
        Route::resource('lessons','LessonsController');
        Route::resource('tags','TagController');
    });
    
    
});

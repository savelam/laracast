<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;

class Lesson extends Model
{
    //
    //
    protected $fillable=[
        'title','body'
    ];
    
//    protected $hidden=['title'];  // used to omit some fields from displaying. Like exclusion in symfony

    /**
     * 
     * @return type
     */
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    
}

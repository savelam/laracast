<?php

use Illuminate\Database\Seeder;
use App\Lesson;
use App\Tag;
use Faker\Factory as Faker;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class LessonTagTableSeeder extends Seeder {

    public function run() {
        // TestDummy::times(20)->create('App\Post');
        $faker = Faker::create();
        $lessonIds = Lesson::lists('id');
        $tagsIds = Tag::lists('id');
        
        foreach (range(1, 10) as $index) {
            DB::table('lesson_tag')->insert([
                'lesson_id'=>$faker->randomElement($lessonIds->toArray()),
                'tag_id'=>$faker->randomElement($tagsIds->toArray())
            ]);
        }
    }

}

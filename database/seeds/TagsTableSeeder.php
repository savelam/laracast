<?php

use Illuminate\Database\Seeder;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Lesson;
use App\Tag;
use Faker\Factory as Faker;

class TagsTableSeeder extends Seeder {

    public function run() {
        // TestDummy::times(20)->create('App\Post');
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            Tag::create([
                'name' => $faker->word
            ]);
        }
    }

}

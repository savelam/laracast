<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
       
        <link href="/css/style.css" rel="stylesheet" type="text/css">

        <style>
            
        </style>
    </head>
    <body>
        <div class="container" >
        <h2>Justin Bieber Official Fun Club</h2>
        @yield('content')
        @yield('footer')
        </div>
    </body>
</html>

@extends('master')

@section('content')


<h3>{{ $song->title }}</h3>
{!! Form::model($song,['route'=>['update_path',$song->slug],'method'=>'PATCH']) !!}

@include('songs.form')

<div class="form-group">

    {!! Form::submit('Update Song',['class'=>'btn btn-primary']) !!}
</div>

{!! Form::close() !!}

{!! Form::open(['route'=>['delete_path',$song->slug],'method'=>'DELETE']) !!}
{!! Form::submit('Delete Song',['class'=>'btn btn-danger']) !!}

{!! Form::close() !!}

@stop


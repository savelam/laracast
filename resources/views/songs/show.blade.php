@extends('master')

@section('content')


<h3>{{ $song->title }}</h3>

@if($song->lyrics)

<div>
    {!! nl2br($song->lyrics) !!}

</div>
{!! link_to_route('songs_path','Go Back Home') !!}
@endif

@stop


